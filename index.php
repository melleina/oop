<?php

	require_once("animal.php");
	require_once("Ape.php");
	require_once("Frog.php");

	$sheep = new Animal("shaun");
	echo "Nama hewan : $sheep->name <br>"; // "shaun"
	echo "Jumlah kaki : $sheep->legs <br>"; // 2
	echo "Hewan berdarah dingin : $sheep->cold_blooded <br><br>"; // false

	$sungokong = new Ape("kera sakti");
	echo "Nama hewan : $sungokong->name <br>";
	echo "Jumlah kaki : $sungokong->legs <br>";
	echo "Hewan berdarah dingin : $sungokong->cold_blooded <br>";
	echo $sungokong->yell();
	echo "<br><br>";

	$kodok = new Frog("buduk");
	echo "Nama hewan : $kodok->name <br>";
	echo "Jumlah kaki : $kodok->legs <br>";
	echo "Hewan berdarah dingin : $kodok->cold_blooded <br>";
	echo $kodok->jump();
	echo "<br><br>";

?>